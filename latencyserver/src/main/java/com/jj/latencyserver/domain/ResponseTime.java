package com.jj.latencyserver.domain;

public class ResponseTime {
	
	private final long id;
    private final long latency;

    public ResponseTime(final long id, final long latency) {
        this.id = id;
        this.latency = latency;
    }

    public long getId() {
        return id;
    }

    public long getLatency() {
        return latency;
    }
}
