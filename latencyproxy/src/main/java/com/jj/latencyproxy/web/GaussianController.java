package com.jj.latencyproxy.web;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.jj.latencyproxy.domain.ResponseTime;

import io.micrometer.core.annotation.Timed;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class GaussianController {
	
	@Value("${latancy.server}")
	private String latencyServer;
	
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/gaussian")
	@ResponseBody
	@Timed(histogram = true)
	public ResponseTime gaussian(@RequestParam(name = "mean_latency", required = true) long mean_latency,
			@RequestParam(name = "sd_latency", required = true) long sd_latency) {
		
		log.trace("GaussianController.gaussian");
		
		final Map<String, Long> params = new HashMap<String, Long>();
	    params.put("mean_latency", mean_latency);
	    params.put("sd_latency", sd_latency);
		
		final ResponseTime responseTime = restTemplate.getForObject(
				latencyServer + "/gaussian?mean_latency={mean_latency}&sd_latency={sd_latency}", ResponseTime.class, params);
		log.info(responseTime.toString());
		
		return responseTime;
	}
}
