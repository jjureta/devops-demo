package com.jj.latencyproxy.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseTime {
	
	private long id;
    private long latency;

    public ResponseTime() {
        this(0, 0);
    }
    
    public ResponseTime(final long id, final long latency) {
        this.id = id;
        this.latency = latency;
    }

    public long getId() {
        return id;
    }
    
    public void setId(final long id) {
    	this.id = id;
    }

    public long getLatency() {
        return latency;
    }
    
    public void setLatency(final long latency) {
    	this.latency = latency;
    }
    
    @Override
    public String toString() {
      return "ResponseTime{" +
          "id='" + id + '\'' +
          ", latency=" + latency +
          '}';
    }
}
